﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8
{
    public class Player
    {
        public static char player='O';
        public static char GetPlayer()
        {

            if (player == 'O')
            {
                player = 'X';
            }
            else
            {
                player = 'O';
            }

            return player;
        }
    }
}
