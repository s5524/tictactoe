﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp8
{
    public class AiTTT
    {
        public int[,] moveAi ;

        //W tej metodzie tworze kolejna tablice ktora bedzie nam słuzyla do zapisywania wartosc pol w jakich komputer moze wykonac ruch
        //na starcie sprawdza ktore pola sa zajete przez gracza i daje im wartosc -1 aby nigdy ich nie wytypowac
        //natomiast pola w ktorych mozna dokonac ruchu maja wartosc 0
        //zbiera dane do tablicy szuka najwyzszej wartosci i robi liste z tych wartosci
        //i losowo wybiera pole z maksymalna iloscia punktów
        //jezeli sa same zera to losowo wybiera wolne pole na planszy bo zero to najwyzszy punkt
        //algorytm sprawdza każde pole '_' i jezeli wokól tego pola jestfigura komputera dodaje punkt i sprawdza kolejne pole za figura jak sa 2 
        //to dodaje kolejny punk i tak w kazdym kierunku
        //w ostatniej część jest metoda która przekazuje z maciezy punktow pola z najwyzsza liczba punktow i traktuje to jako ruch AI
        
        
        

        public void AiMove(char[,] list, int winninPoints)
        {
            int counter = 0;
            var listLenght = list.GetLength(0);
            moveAi = new int[listLenght, listLenght];
            //ListAiMpves();
            for (int i = 0; i < listLenght; i++)
            {
                for (int j = 0; j < listLenght; j++)
                {
                    if (list[i,j]=='X')
                    {
                        moveAi[i, j] = -1;
                    }
                    
                }
            }
            for (int i = 0; i < listLenght; i++)
            {
                for (int j = 0; j < listLenght; j++)
                {
                    if (list[i, j] == '_')
                    {
                        if (j+1<listLenght && list[i,j+1]=='O')
                        {
                            //moveAi[i, j]++;
                            for (int k = j+1; k < listLenght; k++)
                            {
                                if (k<listLenght&&list[i,k]=='O')
                                {
                                    moveAi[i, j]++;
                                }
                                else
                                {
                                    k = listLenght;
                                }
                            }
                        }
                    }
                }
            }
            for (int i = 0; i < listLenght; i++)
            {
                for (int j = 0; j < listLenght; j++)
                {
                    if (list[i, j] == '_')
                    {
                        if (j - 1 >= 0 && list[i, j - 1] == 'O')
                        {
                            //moveAi[i, j]++;
                            for (int k = j - 1; k >= 0; k--)
                            {
                                if (k < listLenght && list[i, k] == 'O')
                                {
                                    moveAi[i, j]++;
                                }
                                else
                                {
                                    k = 0;
                                }
                            }
                        }
                    }
                }
            }

            for (int i = 0; i < listLenght; i++)
            {
                for (int j = 0; j < listLenght; j++)
                {
                    if (list[i, j] == '_')
                    {
                        if (i + 1 < listLenght && list[i+1, j] == 'O')
                        {
                            //moveAi[i, j]++;
                            for (int k = i + 1; k < listLenght; k++)
                            {
                                if (k < listLenght && list[k, j] == 'O')
                                {
                                    moveAi[i, j]++;
                                }
                                else
                                {
                                    k = listLenght;
                                }
                            }
                        }
                    }
                }
            }

            for (int i = 0; i < listLenght; i++)
            {
                for (int j = 0; j < listLenght; j++)
                {
                    if (list[i, j] == '_')
                    {
                        if (i - 1 >= 0 && list[i-1, j] == 'O')
                        {
                            for (int k = i - 1; k >= 0; k--)
                            {
                                if (k < listLenght && list[k, j] == 'O')
                                {
                                    moveAi[i, j]++;
                                }
                                else
                                {
                                    k = 0;
                                }
                            }
                        }
                    }
                }
            }

            AiTakeMOve(moveAi);
        }

        private void AiTakeMOve(int[,] ints)
        {
            List<MoveList> list = new List<MoveList>();
            for (int i = 0; i < ints.GetLength(0); i++)
            {
                for (int j = 0; j < ints.GetLength(1); j++)
                {
                   var moveList = new MoveList();
                    if (ints[i,j]>=0)
                    {
                        moveList.posX = i;
                        moveList.posY = j;
                        moveList.value = ints[i, j];
                        list.Add(moveList);
                    }
                }
            }
            var maxValue = list.Max(x=>x.value);
            var movesList =list.FindAll(x=>x.value==maxValue);
            var random = new Random();
            var index = random.Next(movesList.Count);
            var cell = new Cells();
            cell.ChangeValue(movesList[index].posX, movesList[index].posY, Player.GetPlayer());
            list.Clear();
        }

        public void ListAiMpves()
        {

            for (int i = 0; i < moveAi.GetLength(0); i++)
            {
                for (int j = 0; j < moveAi.GetLength(0); j++)
                {
                    moveAi[i, j] = 0;
                }
            }
        }

        public void PrintList()
        {
            for (int i = 0; i < moveAi.GetLength(0); i++)
            {
                for (int j = 0; j < moveAi.GetLength(0); j++)
                {
                    Console.Write(moveAi[i,j]+"   ");
                }
                Console.WriteLine();
            }
        }
    }
}
