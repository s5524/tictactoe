﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8
{
    public class Cells
    {
        public static char[,] list;

        public char[,] GetList(int x)
        {
            list = new char[x, x];

            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < x; j++)
                {
                    list[i, j] = '_';
                }
            }
            return list;

        }



        public bool ChangeValue(int x, int y, char c)
        {
            if (list[x, y] != 'X' && list[x, y] != 'O')
            {
                list[x, y] = c;
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool IsFreeCells()
        {
            int counter = 0;
            for (int i = 0; i < list.GetLength(0); i++)
            {
                for (int j = 0; j < list.GetLongLength(1); j++)
                {
                    if (list[i, j] == '_')
                    {
                        counter++;
                    }
                }
            }
            if (counter == 0)
            {
                return false;
            }
            return true;
        }

        public bool IsWinner(int winnerPoints)
        {
            List<int> player1points = new List<int>();
            List<int> player2points = new List<int>();


            for (int i = 0; i < list.GetLength(0); i++)
            {
                player2points.Clear();
                player1points.Clear();
                for (int j = 0; j < list.GetLongLength(1); j++)
                {

                    if (list[i, j] == 'X')
                    {
                        player1points.Add(1);
                        player2points.Clear();
                    }
                    if (list[i, j] == 'O')
                    {
                        player2points.Add(1);
                        player1points.Clear();
                    }
                    if (list[i, j] == '_')
                    {
                        player2points.Clear();
                        player1points.Clear();
                    }
                    if (player1points.Count == winnerPoints || player2points.Count == winnerPoints)
                    {
                        return true;
                    }
                }

            }

            for (int i = 0; i < list.GetLength(0); i++)
            {
                player2points.Clear();
                player1points.Clear();
                for (int j = 0; j < list.GetLongLength(1); j++)
                {

                    if (list[j, i] == 'X')
                    {
                        player1points.Add(1);
                        player2points.Clear();
                    }
                    if (list[j, i] == 'O')
                    {
                        player2points.Add(1);
                        player1points.Clear();
                    }

                    if (list[j, i] == '_')
                    {
                        player2points.Clear();
                        player1points.Clear();
                    }
                    if (player1points.Count == winnerPoints || player2points.Count == winnerPoints)
                    {
                        return true;
                    }
                }

            }

            for (int i = 0; i < list.GetLength(0) - winnerPoints+1; i++)
            {
                player2points.Clear();
                player1points.Clear();

                for (int j = 0; j < list.GetLongLength(1) - winnerPoints+1; j++)
                {

                    for (int k = 0; k < winnerPoints; k++)
                    {


                        if (list[i + k, j + k] == 'X')
                        {
                            player1points.Add(1);
                            player2points.Clear();
                        }
                        if (list[i + k, j + k] == 'O')
                        {
                            player2points.Add(1);
                            player1points.Clear();
                        }

                        if (list[i + k, j + k] == '_')
                        {
                            player2points.Clear();
                            player1points.Clear();
                        }
                        if (player1points.Count == winnerPoints || player2points.Count == winnerPoints)
                        {
                            return true;
                        }
                    }
                }

            }


            for (int i = winnerPoints-1; i < list.GetLength(0); i++)
            {
                
                for (int j = 0; j < list.GetLength(0); j++)
                {
                    player2points.Clear();
                    player1points.Clear();
                    for (int k = 0; k < list.GetLength(0); k++)
                    {
                        
                        if (i -k>= 0 && j +k< list.GetLength(0))
                        {
                            if (list[i - k, j + k] == 'X')
                            {
                                player1points.Add(1);
                                player2points.Clear();
                            }
                            if (list[i - k, j + k] == 'O')
                            {
                                player2points.Add(1);
                                player1points.Clear();
                            }

                            if (list[i - k, j + k] == '_')
                            {
                                player2points.Clear();
                                player1points.Clear();
                            }
                            if (player1points.Count == winnerPoints || player2points.Count == winnerPoints)
                            {
                                return true;
                            }
                        }
                    }
                }
                
            }
            return false;

        }
    }
}
