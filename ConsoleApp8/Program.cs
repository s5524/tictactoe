﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8
{
    class Program
    {
        Matrix matrix = new Matrix();
        Cells cells = new Cells();
        AiTTT ai = new AiTTT();


        static void Main(string[] args)
        {
            var program = new Program();
            program.GameMenu();
        }

        public void GameMenu()
        {
            while (true)
            {
                Player.player = 'O';
                Console.Clear();
                Console.WriteLine("Witaj w grze........");
                Console.WriteLine("1: Gra dla dwóch graczy 2: Gra z AI 3: Wyjdz z gry");
                var gameChoice = IoHelpers.IOHelp.GetNumber(Console.ReadLine());
                if (gameChoice==3)
                {
                    break;
                }
                Console.WriteLine("Podaj wielkość planszy od 3-10");
                cells.GetList(IoHelpers.IOHelp.GetMatrixLenght(Console.ReadLine()));
                Console.WriteLine("Podaj ilosc figur w rzedzie by wygrac");
                StartGame(IoHelpers.IOHelp.GetWiningLenght(Console.ReadLine()),gameChoice);

            }
        }

        public void StartGame(int winingScore,int aiChose)
        {
            
            while (true)
            {
                Console.Clear();
                matrix.PrintMatrix(Cells.list);
                Console.WriteLine("Podaj współżędne miejsca gdzie chcesz postawic");
                ChangeCell();
                if (aiChose!=1)
                {
                    ai.AiMove(Cells.list,winingScore);
                }
                Console.Clear();
                if (cells.IsWinner(winingScore))
                {
                    Console.Clear();
                    Console.WriteLine("Gratulacje wygrał gracz grający {0} \n Dowolny klawisz aby zacząc od nowa",Player.player);
                    Console.ReadKey();
                    return;
                }
                if (cells.IsFreeCells() == false)
                {
                    Console.Clear();
                    Console.WriteLine("REMIS!!! Dowolny klawisz aby zacząc od nowa");
                    Console.ReadKey();
                    return;
                }
              
            }
        }

        

        public void ChangeCell()
        {
            while (true)
            {
                var values = IoHelpers.IOHelp.GetXAndY(Console.ReadLine());

                if (cells.ChangeValue(values[0] , values[1], Player.GetPlayer()))
                {
                    return;
                }
                else
                {
                    Console.WriteLine("Pole juz zajęte");
                }

            }
        }
        
    }
}
