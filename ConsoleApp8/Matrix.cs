﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8
{
    class Matrix
    {
        public void PrintMatrix(char[,] list)
        {
            var demension = Math.Sqrt(list.Length);
            char c;
            Console.Write(" ");
            for (var i = 0; i < demension; i++)
            {
                Console.Write("  " + (i) + " ");
            }
            Console.WriteLine();
            Console.Write(" ");
            for (var j = 0; j < demension; j++)
            {
                
                Console.Write(" ");
                for (var i = 0; i < demension; i++)
                {
                    Console.Write("----");
                }
                Console.WriteLine();
                Console.Write(" ");
                for (var i = 0; i < demension; i++)
                {
                    Console.Write("|   ");
                }
                Console.WriteLine("|");
                Console.Write("{0}",c= (char)(j + 65));
                for (var i = 0; i < demension; i++)
                {
                    Console.Write("| {0} ",list[j,i]);//cialo
                }
                Console.WriteLine("|");
                Console.Write(" ");
                for (var i = 0; i < demension; i++)
                {
                    Console.Write("|   ");
                }
                Console.WriteLine("|");
                Console.Write(" ");
            }
            Console.Write(" ");
            for (var i = 0; i < demension; i++)
            {
                Console.Write("----");
            }

            Console.Write("\n");
        }
    }
}
