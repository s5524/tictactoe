﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8.IoHelpers
{
    public class IOHelp
    {
        public static int GetMatrixLenght(string matrix)
        {
            int number;
            while (true)
            {
                if (int.TryParse(matrix, out number))
                {
                    if (number>2&&number<11)
                    {
                    return number;
                    }
                    else
                    {
                        Console.WriteLine("Podaj liczbe od 3 do 10");
                        matrix = Console.ReadLine();
                    }
                }
                else
                {
                    Console.WriteLine("Podaj liczbe od 3 do 10");
                    matrix = Console.ReadLine();
                }

            }
        }

        public static int GetWiningLenght(string winingLenght)
        {
            int number;
            while (true)
            {
                if (int.TryParse(winingLenght, out number))
                {
                    if (number > 2 && number <= Cells.list.GetLength(0))
                    {
                        return number;
                    }
                    else
                    {
                        Console.WriteLine("Podaj liczbe od 3 do {0}",Cells.list.GetLength(0));
                        winingLenght = Console.ReadLine();
                    }
                }
                else
                {
                    Console.WriteLine("Podaj liczbe od 3 do {0}",Cells.list.GetLength(0));
                    winingLenght = Console.ReadLine();
                }

            }
        }

        public static int GetNumber(string numbers)
        {
            int number;
            while (true)
            {
                if (int.TryParse(numbers, out number))
                {
                    if (number > 0 && number <= 3)
                    {
                        return number;
                    }
                    else
                    {
                        Console.WriteLine("Podaj liczbe od 1 do 3");
                        numbers = Console.ReadLine();
                    }
                }
                else
                {
                    Console.WriteLine("Podaj liczbe od 1 do 3");
                    numbers = Console.ReadLine();
                }

            }
        }
        public static List<int> GetXAndY(string numbersXAndY)
        {
            List<int> numbers = new List<int>(2);
            
            while (true)
            {
                var chars = numbersXAndY.ToCharArray();
                if (chars.Length==2)
                {
                    int dupa = chars[0]-65;
                    if (int.TryParse(dupa.ToString(), out var number0))
                    {
                        if (number0>=0 && number0 < Cells.list.GetLength(0))
                        {
                        numbers.Add(number0);
                        }
                    }
                    if (int.TryParse(chars[1].ToString(), out var number1))
                    {
                        if (number1 >= 0 && number1 < Cells.list.GetLength(0))
                        {
                            numbers.Add(number1);
                        }
                    }
                    if (numbers.Count==2)
                    {
                        return numbers;
                    }
                    if (numbers.Count!=2)
                    {
                        Console.WriteLine("Podaj poprawne wspolzedne");
                        numbers.Clear();
                        numbersXAndY = Console.ReadLine();
                    }
                }
                if (chars.Length!=2)
                {
                    Console.WriteLine("Podaj poprawne wspolzedne");
                    numbers.Clear();
                    numbersXAndY = Console.ReadLine();
                }
                
            }
        }
    }
}
